package com.nio.netty.inandout;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * void 表明无需判断类型
 */
public class MyByteToLongDecoder2 extends ReplayingDecoder<Void> {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        System.out.println("------------MyByteToLongDecoder2 被调用！------------");
        /*此处无需判断数据长度，ReplayingDecoder内部会进行管理*/
            out.add(in.readLong());
    }
}
