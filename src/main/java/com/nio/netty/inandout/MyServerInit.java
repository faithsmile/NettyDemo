package com.nio.netty.inandout;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

public class MyServerInit extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        /*这个地方加编码器和解码器，不冲突，出站时，只走编码器，入站时，只走解码器*/
        /*入站的handler进行解码，MyByteToLongDecoder*/
//        pipeline.addLast(new MyByteToLongDecoder());
        pipeline.addLast(new MyByteToLongDecoder2());
        /*添加编码handler，回写数据给客户端，MyLongToByteEncoder*/
        pipeline.addLast(new MyLongToByteEncoder());
        /*自定义的入站handler处理业务逻辑*/
        pipeline.addLast(new MyServerHandler());
    }
}
