/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.nio.netty.sources.echo2;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.EventLoopGroup;
import io.netty.util.concurrent.DefaultEventExecutor;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

import java.util.concurrent.Callable;

import static io.netty.util.CharsetUtil.UTF_8;

/**
 * Handler implementation for the echo server.
 */
@Sharable
public class EchoServerHandler extends ChannelInboundHandlerAdapter {
    /*
    * group 就是充当业务线程池，可以将任务提交到该线程池
    * 这里创建了16个线程
    * */
    static final EventExecutorGroup group=new DefaultEventExecutorGroup(16);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        System.out.println("EchoServerHandler 的线程是："+Thread.currentThread().getName());
        /*模拟出现了耗时的操作*/
        /*解决方案1，用户自定义的普通任务*/
        /*这种方式，其实并没有实现异步，两个任务都是加到同一个线程中执行的*/
        /*
        ctx.channel().eventLoop().execute(()->{
            try {
                Thread.sleep(5*1000);
                System.out.println("EchoServerHandler execute的线程是："+Thread.currentThread().getName());
                ctx.writeAndFlush(Unpooled.copiedBuffer("hello ,客户端~**************！",UTF_8));
//                System.out.println("channel code="+ctx.channel().hashCode());
            } catch (InterruptedException e) {
                System.err.println("发生异常："+e.toString());;
            }
        });

        ctx.channel().eventLoop().execute(()->{
            try {
                Thread.sleep(5*1000);
                System.out.println("EchoServerHandler execute的线程2是："+Thread.currentThread().getName());
                ctx.writeAndFlush(Unpooled.copiedBuffer("hello ,客户端~**************！",UTF_8));
//                System.out.println("channel code="+ctx.channel().hashCode());
            } catch (InterruptedException e) {
                System.err.println("发生异常："+e.toString());;
            }
        });
        */

        /*解决方案2，将任务提交到group线程池，这个线程池是定义在handler中的*/
        /*
        * 此时，workerGroup 是个线程池，里面有多个线程来管理handler
        * 没个handler中又有一个线程池group，来处理并发任务
        * 所以，多个客户端连接到server时，EchoServerHandler 的线程 是不同的
        * 如果每个handler中有多个任务，那么，每个group.submit 的 call 线程也是不同的
        * */
        group.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                /*接受客户端消息*/
                ByteBuf buf= (ByteBuf) msg;
                byte[] bytes = new byte[buf.readableBytes()];
                buf.readBytes(bytes);

                String s = new String(bytes, UTF_8);
                /*休眠10秒钟，模拟耗时任务*/
                Thread.sleep(10000);
                System.out.println("group.submit 的 call 线程是："+Thread.currentThread().getName());
                ctx.writeAndFlush(Unpooled.copiedBuffer("hello ,客户端~group.submit！",UTF_8));
                return null;
            }
        });
        System.out.println("go on!");
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        ctx.close();
    }
}
