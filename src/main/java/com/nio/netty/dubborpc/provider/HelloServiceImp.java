package com.nio.netty.dubborpc.provider;

import com.nio.netty.dubborpc.pubinterface.HelloService;

import java.util.Date;

public class HelloServiceImp implements HelloService {
    private static int count=0;
    /*当有消费方调用该方法时，就返回一个结果String*/
    @Override
    public String hello(String msg) {
        System.out.printf("[%tF %<tT] 收到客户端消息："+msg+"\n",new Date());
        /*根据msg，返回不同的结果*/
        if (msg!=null){
            return "你好客户端， 我已经收到了你的消息【"+msg+"】第"+(++count)+"次被调用！";
        }else {
            return "你好客户端， 我已经收到了你的消息[]";
        }
    }
}
