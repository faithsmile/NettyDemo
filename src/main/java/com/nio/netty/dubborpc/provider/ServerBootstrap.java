package com.nio.netty.dubborpc.provider;

import com.nio.netty.dubborpc.netty.NettyServer;

/*该类会启动一个服务启动者，就是NettyServer*/
public class ServerBootstrap {
    public static void main(String[] args) {
        NettyServer.startServer("127.0.0.1",7000);
    }
}
