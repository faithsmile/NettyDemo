package com.nio.netty.dubborpc.pubinterface;

/*这个是接口，是服务提供方，服务消费方都需要的*/
public interface HelloService {
    String hello(String msg);
}
