package com.nio.netty.dubborpc.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.lang.reflect.Proxy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NettyClient {
    /*创建线程池*/
    private static ExecutorService executor= Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private static NettyClientHandler client;

    /*编写方法，使用代理模式，获取代理对象*/
    public Object getBean(final Class<?> serviceClass,final String provider){

        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class<?>[]{serviceClass},(proxy,method,args)->{
            /*{} 部分的代码，客户端每调用一次 hello，就会进入该代码块  */
                    if (client==null){
                        initClient();
                    }
                    /*设置要发给服务器端的信息*/
                    /*provider 是协议头，args[0]就是客户端调用API hello(??) 参数*/
                    client.setPara(provider+args[0]);
                    /*这里的get是获取线程任务执行的返回结果，也就是client handler中call return 的结果*/
                    return executor.submit(client).get();
                });
    }

    /*初始化客户端*/
    private static void initClient(){
        client=new NettyClientHandler();
        /*创建    EventLoopGroup*/
        NioEventLoopGroup group = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY,true)
                .handler(new ChannelInitializer<SocketChannel>() {

                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new StringDecoder());
                        pipeline.addLast(new StringEncoder());
                        pipeline.addLast(client);
                    }
                });
        try {
            ChannelFuture future = bootstrap.connect("127.0.0.1", 7000).sync();
//            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
