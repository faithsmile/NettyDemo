package com.nio.netty.dubborpc.customer;

import com.nio.netty.dubborpc.netty.NettyClient;
import com.nio.netty.dubborpc.pubinterface.HelloService;

import java.util.Date;

public class ClientBootstrap {

    /*此处定义协议头*/
    public static final String provider = "HelloService#hello#";

    public static void main(String[] args) throws InterruptedException {

        /*创建一个消费者*/
        NettyClient customer = new NettyClient();
        /*创建代理对象*/
        HelloService service = (HelloService) customer.getBean(HelloService.class, provider);
        /*通过    代理对象调用  服务  提供者的API*/
        while (true){
            Thread.sleep(2000);
            String hello = service.hello("你好，dubbo！");
            System.out.printf("[%tF %<tT] 调用的结果："+hello+"\n",new Date());
        }

    }
}
