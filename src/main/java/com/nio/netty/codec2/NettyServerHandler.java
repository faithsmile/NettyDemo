package com.nio.netty.codec2;

import com.nio.netty.codec.StudentPOJO;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;

import static io.netty.util.CharsetUtil.UTF_8;

/*
*   说明：
*   1、我们自定义一个Handler  需要继承netty 规定好的某个HandlerAdapter（规范）
*       此时，我们自定义的Handler，才能称为一个Handler
* */
public class NettyServerHandler extends SimpleChannelInboundHandler<MyDataInfo.MyMessage> {

    /*
    * 读取数据事件（这里我们可以读取客户端发送的消息）
    * 1、ChannelHandlerContext ctx:上下文对象，含有 管道pipeline，通道channel，地址
    * 2、Object msg：就是客户端发送的数据 默认Object
    * */
    @Override
    public void channelRead0(ChannelHandlerContext ctx, MyDataInfo.MyMessage msg) throws Exception {
        /*根据dataType  来显示不同的信息*/
        MyDataInfo.MyMessage.DataType dataType = msg.getDataType();
        if (dataType == MyDataInfo.MyMessage.DataType.StudentType){
            MyDataInfo.Student student = msg.getStudent();
            System.out.println("学生 id="+student.getId()+"    学生 name="+student.getName());
        }else if(dataType == MyDataInfo.MyMessage.DataType.WorkerType){
            MyDataInfo.Worker worker = msg.getWorker();
            System.out.println("工人 age="+worker.getAge()+"    工人  name="+worker.getName());
        }else{
            System.out.println("传输的类型不正确！");
        }
    }

    /*数据读取完毕：回复数据*/
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        /*
        * 将数据写入到缓存并刷新
        * 一般讲，我们对这个发送的数据进行编码
        * */
        ctx.writeAndFlush(Unpooled.copiedBuffer("hello ,客户端~旺旺旺！",UTF_8));
    }

    /*处理异常一般是需要关闭通道*/
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
