package com.nio.netty.codec2;

import com.nio.netty.codec.StudentPOJO;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.Random;

import static io.netty.util.CharsetUtil.UTF_8;

public class NettyClientHandler extends ChannelInboundHandlerAdapter {
    /*当通道就绪就会触发该方法：发送消息*/
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        /*随机的发送Student或者 Worker对象*/
        int nextInt = new Random().nextInt(3);
        MyDataInfo.MyMessage myMessage=null;
        if (0==nextInt){
            myMessage = MyDataInfo.MyMessage.newBuilder().setDataType(MyDataInfo.MyMessage.DataType.StudentType)
                    .setStudent(MyDataInfo.Student.newBuilder().setId(5).setName("吴道子 画圣").build()).build();
        }else{
            myMessage = MyDataInfo.MyMessage.newBuilder().setDataType(MyDataInfo.MyMessage.DataType.WorkerType)
                    .setWorker(MyDataInfo.Worker.newBuilder().setAge(20).setName("隔壁老王").build()).build();
        }
        ctx.writeAndFlush(myMessage);
    }

    /*当通道有读取事件时，会触发读取服务器端返回的数据*/
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf buf= (ByteBuf) msg;
        System.out.println("服务器回复的消息："+buf.toString(UTF_8));
        System.out.println("服务器的地址："+ctx.channel().remoteAddress());
    }

    /*处理异常一般是需要关闭通道*/
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
